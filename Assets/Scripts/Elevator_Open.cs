﻿using UnityEngine;
using System.Collections;

public class Elevator_Open : MonoBehaviour {
	private Animator animator;

	void Start(){
		animator = GetComponent<Animator>();
	}

	void OnTriggerEnter(Collider other){
		animator.SetBool ("Move", true);
	}

	void OnTriggerExit(Collider other){
		animator.SetBool ("Move", false);	
	}
}
