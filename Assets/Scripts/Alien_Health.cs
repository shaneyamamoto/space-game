﻿using UnityEngine;
using System.Collections;

public class Alien_Health : MonoBehaviour {
	public float EnemyHealth = 100;
	public float push_distance = 5;
	public float push_speed = 2;


	public GameObject HealthBar;
	public GameObject Parent;
	public GameObject Astronaut;

	public Animator astronaut_animator;
	public Animator animator;

	private float overallhealth;
	private float HealthSize;

	private bool waitActive = false;
	private bool push_astronaut = false;
	private bool melee = false;

	private Vector3 death_position = new Vector3(0,0,0);
	private Vector3 newposition = new Vector3(0,0,0);






	// Use this for initialization
	void Start () {
		overallhealth = EnemyHealth;
		HealthSize = HealthBar.transform.localScale.x;
		animator = GetComponentInParent<Animator>();
		astronaut_animator = Astronaut.GetComponent<Animator> ();
	}
	void Update(){
		if(melee == true){

			animator.SetBool("Push",true);
			push_astronaut = true;
			melee = false;

		}
		else{
			animator.SetBool("Push",false);
		}
		
		if (push_astronaut == true) {
			
			Astronaut.transform.position = Vector3.Lerp (Astronaut.transform.position, newposition, Time.deltaTime * push_speed);
			//astronaut_animator.SetBool("Pushed",true);
			if(Vector3.Distance(Astronaut.transform.position, newposition) < 1)
			{
				
				push_astronaut = false;
				//astronaut_animator.SetBool("Pushed",false);
			}
			if(Input.GetButtonDown("Horizontal"))
			{
				
				push_astronaut = false;
				//animator.SetBool("Pushed",false);
			}
			

		}

		if (EnemyHealth == 0) {
			HealthBar.SetActive(false);
			animator.SetBool("Death",true);
			waitActive = true;
		}
		if (waitActive == true) {
			this.collider.enabled = false;
			CapsuleCollider collider = Parent.GetComponent<CapsuleCollider>();
			BoxCollider collider2 = Parent.GetComponent<BoxCollider>();
			Alien_AI script = Parent.GetComponent<Alien_AI>();
			script.enabled = false;
			collider.enabled = false;
			collider2.enabled = false;

			Parent.rigidbody.useGravity =false;
			//Parent.transform.position = Vector3.Lerp (Parent.transform.position, death_position, Time.deltaTime * .05f);	
		}
		HealthBar.transform.localScale = new Vector3(HealthSize, HealthBar.transform.localScale.y, HealthBar.transform.localScale.z);
	
	}
	// Update is called once per frame
	void OnTriggerEnter(Collider other){
		if (other.gameObject.tag == "Bullet") {

			EnemyHealth = EnemyHealth - 10;
			HealthSize = (EnemyHealth * HealthSize) / (overallhealth);

		}
	
		if (other.gameObject.tag == "Player") {
			newposition = new Vector3(Astronaut.transform.position.x - push_distance, Astronaut.transform.position.y, Astronaut.transform.position.z);
			melee = true;		
		}
	}


}
