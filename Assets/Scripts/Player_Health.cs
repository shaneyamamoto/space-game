﻿using UnityEngine;
using System.Collections;

public class Player_Health : MonoBehaviour {

	public float Health = 100;
	public Animator animator;
	// Use this for initialization
	void Take_Damage(float damage){
		Health -= damage;
	}
	void Start(){
		animator = GetComponentInChildren<Animator>();
	}
	// Update is called once per frame
	void Update () {
//		Debug.Log (Health);
		if(Health <= 0)
		{
			animator.SetBool("Death",true);
			//Play Death Something
		}
	}

	void OnTriggerEnter(Collider other){
		if (other.tag == "Melee") {

			Take_Damage(10);

		}
		if (other.tag == "EnemyBullet") {
			Take_Damage (25);		
		}
		
	}

}
