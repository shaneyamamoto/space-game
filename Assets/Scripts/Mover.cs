﻿using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour {
	public GameObject Astronaut;

	void OnTriggerEnter(Collider other){
		Astronaut.gameObject.transform.parent = gameObject.transform;
	}

	void OnTriggerExit(Collider other){
		Astronaut.gameObject.transform.parent = null;
	}
}
