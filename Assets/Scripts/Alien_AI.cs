﻿using UnityEngine;
using System.Collections;

public class Alien_AI : MonoBehaviour {

	public GameObject Astronaut;
	public float follow_speed = 1;
	public float range_aiming = 5;

	private bool follow = false;
	private float moveDir;
	private Vector3 direction;
	private Vector3 original_position;
	private Animator animator;


	void Face(){
		if(moveDir > 0){
			transform.eulerAngles = (moveDir>0)?Vector3.up * 90:Vector3.zero;
		}
		if(moveDir < 0){
			transform.eulerAngles = (moveDir<0)?Vector3.up * 270:Vector3.zero;
		}
	}
	void Start () {

		original_position = transform.position;
		animator = GetComponent<Animator>();

	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log ("X: "+ moveDir)
		if (follow == true) {

			moveDir = Astronaut.transform.position.x - this.transform.position.x;
			Face ();


			if(Vector3.Distance(transform.position, Astronaut.transform.position) > range_aiming){
				transform.position = Vector3.Lerp (transform.position, Astronaut.transform.position, Time.deltaTime * follow_speed); 
				animator.SetBool ("Ready",false);
				animator.SetBool("Shoot",false);
				animator.SetBool("Moving",true);
			}
			else{
				animator.SetBool ("Moving",false);
				animator.SetBool ("Ready",true);
				animator.SetBool ("Shoot",true);
			}
		}

		else 
		{
			moveDir = original_position.x - this.transform.position.x;

			if(Vector3.Distance(transform.position, original_position) > 1){
				Face ();
				animator.SetBool("Shoot",false);
				animator.SetBool ("Ready",false);
				animator.SetBool("Moving",true);

				transform.position = Vector3.MoveTowards(transform.position, original_position, Time.deltaTime * 3); 	
			}
			else{

				animator.SetBool ("Moving",false);

			}
		}


		//if(Vector3.Distance(transform.position, objective.position) < sensitivity){
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.tag == "Player") {
			follow = true;

		}

	}
	void OnTriggerStay(Collider other){
		if (other.gameObject.tag == "Player") {
			follow = true;
			
		}
		
	}
	void OnTriggerExit(Collider other){
		if (other.gameObject.tag == "Player") {
			follow = false;

		}
	}
}
